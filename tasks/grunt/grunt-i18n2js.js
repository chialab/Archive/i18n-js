/*
 * grunt-i18n2js
 * http://gruntjs.com/
 *
 * Copyright (c) 2015 Chialab s.r.l. <dev@chialab.it> (http://www.chialab.it)
 * Licensed under the MIT license.
 */

module.exports = function(grunt) {
    'use strict';

    grunt.registerMultiTask('i18n2js', 'Convert i18n json to javascript.', function() {
        let len = this.filesSrc.length;
        let outputFile = this.data.dest;
        let output = '';

        function iterateTroughFiles(filename) {
            let split = filename.split('/');
            let lang = split.pop();
            let data = grunt.file.read(filename).replace(/[\n|\r|\t]/mg, '');
            output += `
                <script type="text/javascript" data-source="${filename}">
                    i18n.addResources(${data}, '${lang}');
                </script>\n`;
        }

        for (let x = 0; x < len; x++) {
            iterateTroughFiles(this.filesSrc[x]);
        }

        grunt.file.write(outputFile, output);
    });
};
