'use strict';

module.exports = function(karma) {
    karma.files = karma.files || [];
    karma.files.unshift(
        'node_modules/whatwg-fetch/fetch.js',
        {
            pattern: 'test/**/*.json',
            served: true,
            included: false,
            nocache: true,
        }
    );
};
