# I18N
> Internationalization library.

## Install

[![NPM](https://img.shields.io/npm/v/chialab-i18n.svg)](https://www.npmjs.com/package/chialab-i18n)
```
$ npm i chialab-i18n --save
```
[![Bower](https://img.shields.io/bower/v/chialab-i18n.svg)](https://github.com/chialab/i18n-js)
```
$ bower i chialab-i18n --save
```

## Example
```js
var i18n = new I18N({ defaultLang: 'it' });
i18n.addResource({ login: 'Chi sei?' });
i18n.addResource({ userInfo: { hello: 'Ciao, {0}!' }});
i18n.translate('login'); // => "Chi sei?"
i18n.translate('userInfo.hello', 'Alan'); // => "Ciao, Alan!"
```

## Dev

[![Chialab es6-workflow](https://img.shields.io/badge/project-es6--workflow-lightgrey.svg)](https://github.com/Chialab/es6-workflow)
[![Travis](https://img.shields.io/travis/Chialab/i18n-js.svg?maxAge=2592000)](https://travis-ci.org/Chialab/i18n-js)
[![Code coverage](https://codecov.io/gh/Chialab/i18n-js/branch/master/graph/badge.svg)](https://codecov.io/gh/Chialab/i18n-js)

[![Sauce Test Status](https://saucelabs.com/browser-matrix/chialab-sl-004.svg)](https://saucelabs.com/u/chialab-sl-004)
