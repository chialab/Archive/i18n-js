import { I18N } from '../src/i18n.js';

/* globals describe, before, after, beforeEach, afterEach, it, assert */
describe('Unit: I18N', function() {
    this.timeout(20 * 1000);

    let i18n = new I18N({
        languages: ['en', 'it'],
        defaultLang: 'it',
        path: 'base/test/locales',
        autoLoad: false,
    });

    before((done) => {
        i18n.fetch().then(() => {
            done();
        }, () => {
            done();
        });
    });

    it('should detect the navigator language', () => {
        assert.equal(typeof I18N.detect(), 'string');
    });

    it('should translate `hello` in Italian', () => {
        assert.equal(i18n.translate('hello'), 'Ciao mondo!');
    });

    it('should translate and interpolate `user.hello` in Italian', () => {
        assert.equal(i18n.translate('user.hello', 'Alan'), 'Ciao, Alan!');
    });

    it('should translate the path `status.done` in Italian', () => {
        assert.equal(i18n.translate('status.done'), 'Fatto!');
    });

    it('should use `en` fallback for Italian request of `status.todo`', () => {
        assert.equal(i18n.translate('status.todo'), 'Hurry up!');
    });

    it('should return the `key` for Italian request of `status.fail`', () => {
        assert.equal(i18n.translate('status.fail'), undefined);
    });
});
